## Description :

Un virement est un transfert d'argent d'un compte emetteur vers un compte bénéficiaire ...

**Besoin métier :** 

Ajouter un nouveau usecase versement. Le Versement est un dépôt d'agent sur un compte donné . 

Le versement est une opération trés utile lors des transfert de cash .
 
Imaginez que vous allez à une agence avec un montant de 1000DH et que vous transferez ça en spécifiant le RIB souhaité .
 
L'identifiant fonctionnel d'un compte dans ce cas préçis est le RIB .  


## How to use 
To build the projet you will need : 
* Java 11+ 
* Maven

Build command : 
```
mvn clean install
```

Run command : 
```
./mvnw spring-boot:run 
## or use any prefered method (IDE , java -jar , docker .....)
```

.
